import { readable, writable } from "svelte/store";

export const ApplicationName = writable("");
export const ElementName = writable("");

export const NODE_VARIANT = readable("periNODE 0-10V");