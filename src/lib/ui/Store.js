import { readable, writable } from "svelte/store";

export const sidebar_size = writable(0);

export const SIDEBAR_SIZE_HIDDEN = readable(0);
export const SIDEBAR_SIZE_MIN = readable(270);

export const trigger_el = writable();
export const trigger_elm = writable();
