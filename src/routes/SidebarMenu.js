export let sidebarMenu = [
    {
        name: "",       // headline for a folding menu
        folding: false, // is it a flat or a folding menu
        menu: [
            { link: "/staticfiles/", label: "Home" },
            { link: "/staticfiles/info", label: "Info" }
        ]
    },
    {
        name: "",       // headline for a folding menu
        folding: false, // is it a flat or a folding menu
        menu: [
            { link: "/staticfiles/node_config", label: "Node config" },
            { link: "/staticfiles/security_config", label: "Security config" },
            { link: "/staticfiles/firmware_update", label: "Firmware update" },
        ]
    },
    {
        name: "Documentation",  // headline for a folding menu
        folding: true,          // is it a flat or a folding menu
        menu: [
            { link: "/staticfiles/api_documentation", label: "API documentation" },
            { link: "https://docs.perinet.io", label: "Online documentation" },
            ]
    },
];
